<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">

		<div class="header">
			<a href="./">ホーム</a> <br/><a href="signup">ユーザー新規登録</a>
		</div>
		<br />
		<h3>ユーザー編集一覧</h3>

		<div class="userEditing">

			<c:forEach items="${userList}" var="userList">

						名前：<c:out value="${userList.name}" />
				<br />
						ログインID:<c:out value="${userList.loginId}" />
				<br />
						支店名：<c:out value="${userList.branchName}" />
				<br />
						部署・役職<c:out value="${userList.positionName}" />
				<br />
				<a href="userEditing?userId=${userList.userId}">編集</a>
				<br />
					------------------------------------------------------------
					<br />
			</c:forEach>

		</div>


	</div>
</body>
</html>