<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">
			<br /> <label for="name">個人名</label> <input name="name" id="name" />（個人名を10文字以下で記入してください）
			<br /> <select name="branch">

				<option value="1">本社</option>
				<option value="2">支店Ａ</option>
				<option value="3">支店Ｂ</option>
				<option value="4">支店Ｃ</option>
			</select> <br /> <select name="position">
				<option value="1">総務人事</option>
				<option value="2">情報管理</option>
				<option value="3">支店長</option>
				<option value="4">社員</option>

			</select> <br /> <label for="loginId">ログインID</label> <input
				name="loginId" id="loginId" />（ログインIDは記号を含むすべての半角文字かつ6文字以上20文字以下で記入）
			<br /> <label for="password">登録用パスワード</label> <input name="password"
				type="password" id="password" />（パスワードは半角英数字かつ6文字以上20文字以下で記入してください）
			<br /> <label for="password">確認用パスワード</label> <input
				name="confirmationPassword" type="password"
				id="confirmationPassword" /> （登録用パスワードをもう一度記入してください） <br />ユーザー書き込み権限<input
				type="radio" name="permission" value="Yes" id="ari" /> <label
				for="ari" accesskey="y">有</label><input type="radio"
				name="permission" value="No" id="nashi" /> <label for="nashi"
				accesskey="n">無</label> <br /> <input type="submit" value="登録" />
			<br /> <a href="./">戻る</a>

		</form>
	</div>
</body>
</html>