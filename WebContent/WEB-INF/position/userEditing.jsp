<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>
	ユーザー編集画面
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form action="UserEditing" method="post">


		<input type="hidden" name="id" value="${userEdite.id}"> <br />
		<label for="loginId">ログインID</label> <input name="loginId" id="loginId"
			value="${userEdite.loginId}" /> <br /> <label for="password">パスワード</label>
		<input name="password" type="password" id="password" /> <br /> <label
			for="password">確認用パスワード</label> <input name="confirmationPassword"
			type="password" id="confirmationPassword" /> <br />
			<select name="branch">

			<option value="1" <c:if test = "${userEdite.branch == 1}">selected</c:if>>
			本社</option>
			<option value="2" <c:if test = "${userEdite.branch == 2}">selected</c:if>>支店Ａ</option>
			<option value="3" <c:if test = "${userEdite.branch == 3}">selected</c:if>>支店Ｂ</option>
			<option value="4" <c:if test = "${userEdite.branch == 4}">selected</c:if>>支店Ｃ</option>
		</select> <br /> <select name="position">
			<option value="1" <c:if test = "${userEdite.branch == 1}">selected</c:if>>総務人事</option>
			<option value="2" <c:if test = "${userEdite.branch == 2}">selected</c:if>>情報管理</option>
			<option value="3" <c:if test = "${userEdite.branch == 3}">selected</c:if>>支店長</option>
			<option value="4" <c:if test = "${userEdite.branch == 4}">selected</c:if>>社員</option>

		</select> <br /> <label for="name">名前</label> <input name="name" id="name"
			value="${userEdite.name}" /> <br /> <input type="submit" value="登録" />
		<br /> <a href="./">戻る</a>
	</form>
</body>
</html>