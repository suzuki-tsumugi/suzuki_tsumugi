<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="form-area">
	<a href="./">ホーム</a>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="newPosts" method="post">
			件名：<input type="text" name="subject" size="50" >30文字まで<br />
			本文：<br />
			<textarea name="text" cols="50" rows="20"></textarea>
			<br /> 1000文字まで<br /> カテゴリー：<input type="text" name="category"
				size="10" >10文字以内<br /> <input type="submit"
				value="投稿する">

		</form>
	</div>
</body>
</html>