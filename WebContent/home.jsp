<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>

			</c:if>

			<c:if test="${not empty loginUser && loginUser.position == 1}">
				<a href="./">ホーム</a>
				<a href="newPosts">新規投稿</a>
				<a href="userManagements">ユーザー管理</a>
				<a href="logout">ログアウト</a>
			</c:if>
			<c:if test="${not empty loginUser && loginUser.position != 1}">
				<a href="./">ホーム</a>
				<a href="newPosts">新規投稿</a>
				<a href="logout">ログアウト</a>

			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h3>
						<c:out value="${loginUser.name}" />
						でログインしています。
					</h3>
				</div>

				<%--カテゴリー絞込み検索機能 --%>
				<form method="get">
					カテゴリ検索：<input type="text" name="searchWord"> <input
						type="submit" value="絞込" /><br />
				</form>
			</div>
			<%--投稿日（期間指定）検索 --%>
			<form method="get">
				投稿日検索<input type="date" name="searchPosted"></input> <input
					type="submit" value="絞込" />
			</form>
			<br/>
			<br/>
			<br/>
			<%--投稿一覧表示 --%>

			<div class="postList">
				<c:forEach items="${messages}" var="message">

					<div class="message">

						<div class="subject">
							件名：
							<c:out value="${message.subject}" />
						</div>
						<div class="text">
							本文：
							<c:out value="${message.text}" />
						</div>
						<div class="category">
							カテゴリー：
							<c:out value="${message.category}" />
						</div>
						<div class="account-name">
							投稿者： <span class="name"><c:out value="${message.name}" /></span>
						</div>
						<div class="date">
							投稿日時：
							<fmt:formatDate value="${message.created_date}"
								pattern="yyyy年MM月dd日 HH時mm分" />
						</div>

						<%--自分の投稿を削除 --%>
						<c:if test="${message.userId == loginUser.id }">
							<form action="postDelete" method="post">

								<input type="hidden" name="postDelete" value="${message.id}">
								<input type="submit" value="削除">
							</form>
						</c:if>

						<br /> <br />
						<%-- コメント一覧表示 --%>
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId}">
								<div class="comment">

									コメント：
									<c:out value="${comment.comment}" />
									<div class="account-name">
										投稿者： <span class="name"><c:out value="${comment.name}" /></span>
									</div>
									<div class="date">
										投稿日時：
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy年MM月dd日 HH時mm分" />
									</div>
								</div>
								<%-- コメント削除機能実装 --%>
								<c:if test="${comment.userId == loginUser.id}">
									<form action="commentDelete" method="post">
										<input type="hidden" name="commentDelete"
											value="${comment.id}"> <input type="submit"
											value="削除">
									</form>
								</c:if>
							</c:if>
						</c:forEach>
						<%--コメント機能を実装 --%>
						<div class="comment-area">
							<form action="comment" method="post">
								<input type="hidden" name="messageId" value="${message.id }">
								コメント：(500文字以内)<br />
								<textarea name="comment" cols=100 rows=6 class="comment-box"></textarea>
								<input type="submit" value="コメントする">
							</form>
							<%-- コメントバリデーションメッセージ表示--%>
							<c:if test="${ not empty errorMessages }">
								<div class="errorMessages">
									<ul>
										<c:forEach items="${errorMessages}" var="message">
											<li><c:out value="${message}" />
										</c:forEach>
									</ul>
								</div>
								<c:remove var="errorMessages" scope="session" />
							</c:if>
						</div>
					</div>
				</c:forEach>
				<br /> <br />
			</div>
		</c:if>



	</div>
</body>
</html>