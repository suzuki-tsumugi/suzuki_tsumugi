package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.CommentList;
import beans.PostList;
import beans.User;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//  メッセージを取得し、リクエストにメッセージをセットするコードを追加
		User user = (User) request.getSession().getAttribute("loginUser");
		//		userクラスの変数userにセッションで保持していたログインしているユーザーの値を入れる

		boolean isShowPostListForm;//booleanのisShowPostListFormを定義
		if(user != null){ //ログインしていたらtrueを返す
			isShowPostListForm = true;
		}else{
			isShowPostListForm = false;
		}


		String searchWord = request.getParameter("searchWord");//絞込み検索のリクエストパラメーターを取得
		String searchPosted = request.getParameter("searchPosted");

		if(StringUtils.isNotEmpty(searchWord)){
			List<PostList> searchResult = new MessageService().searchCategory(searchWord);
			request.setAttribute("messages",searchResult);
		} else if(StringUtils.isNotEmpty(searchPosted)){
			List<PostList> searchResult = new MessageService().searchPosted(searchPosted);
			request.setAttribute("messages",searchResult);
		} else {
			List<PostList> messages = new MessageService().getPostList();
			//		投稿一覧リストをmessagesリストに入れる
			request.setAttribute("messages", messages);
		}

		//コメント一覧リストをcommentsリストに入れる
		List<CommentList> comments = new CommentService().getCommentList();
		request.setAttribute("comments", comments);

		//

		request.setAttribute("isShowPostListForm", isShowPostListForm);
		//requestに属性"isShowPostListForm"とその値isShowPostListFormをセットする

		request.getRequestDispatcher("/home.jsp").forward(request, response);
		//		フォワードで"/home.jsp"へ処理を移す



	}


}
