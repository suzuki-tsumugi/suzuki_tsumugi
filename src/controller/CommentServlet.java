package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;


@WebServlet(urlPatterns ={"/comment"})
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();//session取得

		List<String> messages = new ArrayList<String>();//バリデーションメッセージを入れるリストを予め用意

		if(isValid(request,messages) == true){//問題無ければ実行
			User user = (User)session.getAttribute("loginUser");//ログインユーザーの値を取得

			Comment comment = new Comment(); // commentクラスのインスタンスを生成
			comment.setComment(request.getParameter("comment"));//入力したコメントをComment.beansにセット
			comment.setUserId(user.getId());//ログインユーザーのidの値をcommentテーブルのユーザーIDと紐付け
			comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));//メッセージのidの値をcommentテーブルのmessage_idを紐付けたい
				
			new CommentService().register(comment);//CommentService経由で登録処理する
			
			response.sendRedirect("./");//ホーム画面へリダイレクト
		}else{
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");//エラーメッセージ表示でもう一度ホーム画面へ
		}

	}


	private boolean isValid(HttpServletRequest request, List<String> messages){

		String comment = request.getParameter("comment");//リクエストパラメータの値の取得
		if (StringUtils.isEmpty(comment)){
			messages.add("コメントを入力してください");
		}
		if(comment.length() > 500){
			messages.add("500文字以内で入力してください");
		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}

	}
}


