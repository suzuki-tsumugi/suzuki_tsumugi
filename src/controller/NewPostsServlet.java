package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;


@WebServlet(urlPatterns= {"/newPosts"})
public class NewPostsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/newPost.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();//セッション取得

		List<String> messages = new ArrayList<String>();//メッセージを入れるリストを予め用意

		if (isValid(request, messages) == true) { //1個もエラーが無くメッセージが無ければtrueで実行
			//object型からUser型にキャスト
			User user = (User) session.getAttribute("loginUser");//セッション中のloginUserの値をobject型の戻り値として返す

			Message message = new Message();
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());//idをユーザーＩｄに紐付け

			new MessageService().register(message); //登録

			response.sendRedirect("./"); //ホーム画面へ
		} else {
			session.setAttribute("errorMessages", messages);//セッションにエラーメッセージを入れる
			response.sendRedirect("newPosts");//エラーメッセージ表示でもう一度新規投稿画面
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(subject) == true) {
			messages.add("件名を入力してください");
		}
		if (subject.length() >= 31) {
			messages.add("30文字以下で入力してください");
		}

		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください");
		}
		if (text.length() >= 1001) {
			messages.add("1000文字以下で入力してください");
		}

		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (category.length() >= 11) {
			messages.add("10文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
