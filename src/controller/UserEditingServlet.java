package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class UserEditingServlet
 */
@WebServlet(urlPatterns = {"/userEditing"})
public class UserEditingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();//セッション取得

		session.setAttribute("userEdite", new UserService().getUser(request.getParameter("userId")));//セッションに"userEdite"を格納
		//URLパラメーターで取得した値を元にUser情報を取得、SetAttributeしてuserEditeにインスタンスを保存する
		request.getRequestDispatcher("/WEB-INF/position/userEditing.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();//session取得
		List<String> messages = new ArrayList<String>();


		if(isValid(request,messages) == true){
			User user = new User(); //userインスタンスの生成
			//			以下インスタンスフィールドへの値の代入
			user.setId(Integer.parseInt(request.getParameter("id")));
			user.setName(request.getParameter("name"));//名前
			user.setBranch(Integer.parseInt(request.getParameter("branch")));//支店
			user.setPosition(Integer.parseInt(request.getParameter("position")));//部署・役職
			user.setLoginId(request.getParameter("loginId"));//ログインID

			if(StringUtils.isEmpty(request.getParameter("password"))){

				new UserService().update(user);


			} else {
				user.setPassword(request.getParameter("password"));//登録用パスワード
				user.setConfirmationPassword(request.getParameter("confirmationPassword"));
				new UserService().update1(user);
			}
			request.getRequestDispatcher("/userManagements").forward(request, response);

		}else{
			session.setAttribute("errorMessages", messages);
//			request.getRequestDispatcher("/userManagements").forward(request, response);
			response.sendRedirect("/WEB-INF/position/UserEditing.jsp");

		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");


		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}

		if(name.length() >= 11){
			messages.add("名前は10文字以下で記入してください");
		}

		if((branch != 1 && position == 1) || (branch != 1 && position == 2)){
			messages.add("この部署・役職は本社勤務の方が対象です");
		}
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(!loginId.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}")){
			messages.add("ログインIDは記号を含むすべての半角文字かつ6文字以上20文字以下で記入してください");
		}
		if(StringUtils.isNotEmpty(request.getParameter("password"))){
			if(!password.matches("[A-Za-z0-9]{6,20}")){//
				messages.add("パスワードは半角英数字かつ6文字以上20文字以下で記入してください");
			}
		}

		if(!(password.equals(confirmationPassword))){
			messages.add("登録用パスワードと確認用パスワードが一致しません");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
