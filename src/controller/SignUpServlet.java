package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;




	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/position/signup.jsp").forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();//session取得
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));//名前
			user.setBranch(Integer.parseInt(request.getParameter("branch")));//支店
			user.setPosition(Integer.parseInt(request.getParameter("position")));//部署・役職
			user.setLoginId(request.getParameter("loginId"));//ログインID
			user.setPassword(request.getParameter("password"));//登録用パスワード
			user.setConfirmationPassword(request.getParameter("confirmationPassword"));//確認用パスワード
			user.setPermission(request.getParameter("permission"));//ユーザー書き込み権限


			new UserService().register(user);

			response.sendRedirect("./");

		}else{
			session.setAttribute("errorMessages", messages);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/position/signup.jsp");
	        dispatcher.forward(request, response);
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String permission = request.getParameter("permission");


		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}

		if(name.length() >= 11){
			messages.add("名前は10文字以下で記入してください");
		}

		if((branch != 1 && position == 1) || (branch != 1 && position == 2)){
			messages.add("この部署・役職は本社勤務の方が対象です");
		}
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(!loginId.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}")){
			messages.add("ログインIDは記号を含むすべての半角文字かつ6文字以上20文字以下で記入してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if(!password.matches("[A-Za-z0-9]{6,20}")){//
			messages.add("パスワードは半角英数字かつ6文字以上20文字以下で記入してください");
		}
		if (StringUtils.isEmpty(confirmationPassword) == true) {
			messages.add("確認用パスワードを入力してください");
		}
		if(!(password.equals(confirmationPassword))){
			messages.add("登録用パスワードと確認用パスワードが一致しません");
		}
		if (StringUtils.isEmpty(permission) == true) {
			messages.add("ユーザー書き込み権限を入力してください");
		}



		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
