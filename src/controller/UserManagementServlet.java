package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserList;
import service.UserService;


@WebServlet(urlPatterns = {"/userManagements"})
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
//	//  メッセージを取得し、リクエストにメッセージをセットするコードを追加
//			User user = (User) request.getSession().getAttribute("loginUser");
//			//		userクラスの変数userにセッションで保持していたログインしているユーザーの値を入れる

//		ユーザーリスト一覧をuserListリストに入れる

		List<UserList> userList = new UserService().getUserList();
		request.setAttribute("userList", userList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/position/userManagements.jsp");
        dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<UserList> userList = new UserService().getUserList();
		request.setAttribute("userList", userList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/position/userManagements.jsp");
        dispatcher.forward(request, response);
	}
}
