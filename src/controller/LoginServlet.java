package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;


@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		String loginId = request.getParameter("loginId");//login.jspで記入したloginIdをString型で取得
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);//ログインサービスクラスで取得した値


		HttpSession session = request.getSession();//セッション取得

		if (user != null) {

			session.setAttribute("loginUser", user);//セッションに"loginUser"を格納
			
			response.sendRedirect("./");//ホーム画面へ遷移
		} else {

			List<String> messages = new ArrayList<String>();
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);//セッションに"errorMessages"という名前をつけたString型のarrayListを格納する
			response.sendRedirect("login");//もう一度ログイン画面へ
		}

	}
}
