package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;


@WebServlet(urlPatterns = {"/postDelete"})
public class PostDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		HttpSession session = request.getSession();//session取得
		
		Message messageDelete = new Message();
		messageDelete.setId(Integer.parseInt(request.getParameter("postDelete")));
		
		new MessageService().delete(messageDelete); //削除
		
		response.sendRedirect("./"); //ホーム画面へ
	}
	

}
