package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exception.SQLRuntimeException;

/**
 * DB(コネクション関係)のユーティリティー
 */
public class DBUtil {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/suzuki_tsumugi";
	private static final String USER = "root";
	private static final String PASSWORD = "Dolce.1109";

	static {

		try {
			Class.forName(DRIVER);//DRIVERドライバの読み込み
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * コネクションを取得します。
	 *
	 * @return
	 */
	public static Connection getConnection() { //データベース接続メソッド

		try {
			//        	指定されたデータベースの URL への接続を試みます。DriverManager は、登録された JDBC ドライバの集合から適切なドライバを選択しようとします。
			Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

			connection.setAutoCommit(false);//自動コミットモードをfalseで無効にする

			return connection;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * コミットします。
	 *
	 * @param connection
	 */
	public static void commit(Connection connection) {

		try {
			//        	直前のコミット/ロールバック以降に行われた変更をすべて永続的なものにし、この Connection オブジェクトが現在保持するデータベースロックをすべて解除
			connection.commit();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * ロールバックします。
	 *
	 * @param connection
	 */
	public static void rollback(Connection connection) {

		if (connection == null) {
			return;
		}

		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}