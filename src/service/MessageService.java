package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.PostList;
import dao.GetMessageDao;
import dao.MessageDao;
;

public class MessageService {

	public void register(Message message) {//登録メソッド
		Connection connection = null;
		try{
			connection = getConnection();//データベースに接続

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);//コミット


		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostList> searchCategory(String searchWord){
		Connection connection = null;
		try{
			connection = getConnection();

			List<PostList> searchResult = new GetMessageDao().searchCategory(connection,searchWord);

			commit(connection);

			return searchResult;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostList> searchPosted(String searchPosted){
		Connection connection = null;
		try{
			connection = getConnection();

			List<PostList> searchResult = new GetMessageDao().searchPosted(connection,searchPosted);

			commit(connection);

			return searchResult;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;//1000件までリストを制限する数字を設定

	public List<PostList> getPostList(){

		Connection connection = null;//コネクション用意
		try{
			connection = getConnection();//コネクション取得

			GetMessageDao messageDao = new GetMessageDao();
			//投稿一覧をDBから取得するためのPostListDaoクラスからインスタンスを生成して変数meesageDaoに
			//に入れる

			List<PostList> ret = messageDao.getPostList(connection, LIMIT_NUM);
			//connectionとLIMIT_NUMを渡し、取得した投稿一覧をリストretに入れる

			commit(connection);//コミット

			return ret;

		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public void delete(Message message)  { //削除

		Connection connection = null;
		try{
			connection = getConnection();//データベースに接続

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, message);

			commit(connection);//コミット
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
