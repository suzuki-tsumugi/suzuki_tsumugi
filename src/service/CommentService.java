package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.CommentList;
import dao.CommentDao;
import dao.CommentListDao;


public class CommentService {
	public void register(Comment comment){ //comment登録メソッド
		Connection connection = null;
		try{
			connection = getConnection();//データベースに接続

			CommentDao commentDao = new CommentDao();//commentDaoのインスタンスを生成して変数commetDaoに入れる
			commentDao.insert(connection, comment);//commentDaoのinsertメソッドを呼び出して実行

			commit(connection);//コミット
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	//コメント一覧を取得するためのコードを追加(表示制限なし)

	public List<CommentList> getCommentList(){
		Connection connection = null;//コネクション用意
		try{
			connection = getConnection();//コネクション取得

			CommentListDao commentDao = new CommentListDao();
//			コメント一覧をDBから取得するためのCommentListDaoクラスのインスタンスを
//			生成して変数commentDaoにいれる
			
			List<CommentList> ret = commentDao.getCommentList(connection);
			//connectionを渡し、取得したコメント一覧をリストretに入れる
			
			commit(connection);//コミット
			
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}



public void delete(Comment commentDelete) { //コメントを削除するための機能

	Connection connection = null;
	try{
		connection = getConnection();
		
		CommentDao commentDao = new CommentDao();
		commentDao.delete(connection,commentDelete);
		
		commit(connection);
	} catch(RuntimeException e){
		rollback(connection);
		throw e;
	}catch(Error e) {
		rollback(connection);
		throw e;
	} finally {
		close(connection);
	}
}
}
