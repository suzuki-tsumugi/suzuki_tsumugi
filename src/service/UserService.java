package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserList;
import dao.UserDao;
import dao.UserListDao;
import utils.CipherUtil;



public class UserService  {




	public void register(User user) { //register(登録)メソッド

		Connection connection = null; //connection

		try {
			connection = getConnection(); //データベースへ接続
			String encPassword = CipherUtil.encrypt(user.getPassword());//SHA-256で暗号化し、バイト配列をBase64エンコーディング
			String encConfirmationPassword = CipherUtil.encrypt(user.getConfirmationPassword());
			user.setPassword(encPassword);
			user.setConfirmationPassword(encConfirmationPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);//コミット
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(String userId) {//ユーザー一覧取得メソッド
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao getUser = new UserDao();

			User user = getUser.getUserList(connection, userId);
			commit(connection);
			return user;

		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserList> getUserList() {//ユーザー一覧取得メソッド(支店名と部署名を結合しているもの)
		Connection connection = null;
		try{
			connection = getConnection();

			UserListDao userList = new UserListDao();

			List<UserList> ret = userList.getUserList(connection);

			commit(connection);
			return ret;
		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update1(User user){ //更新するためのコネクション
		Connection connection = null;
		try{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			String encConfirmationPassword = CipherUtil.encrypt(user.getConfirmationPassword());

			user.setPassword(encPassword);//エンコードしたやつにセットしなおす
			user.setPassword(encConfirmationPassword);


			UserDao userDao = new UserDao();
			userDao.update1(connection, user);

			commit(connection);
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e){
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
	public void update(User user){ //パスワードはそのままで他更新するためのコネクション
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e){
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}







	}
}


