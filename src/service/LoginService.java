package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class LoginService {

	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();//データベースへ接続

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);//passwordを暗号化
			User user = userDao.getUser(connection, loginId, encPassword);

			commit(connection);//コミット(保存処理)

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}