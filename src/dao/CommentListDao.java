
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.CommentList;
import exception.SQLRuntimeException;

public class CommentListDao { //DBからコメント一覧を取得するためのDao
	public List<CommentList> getCommentList(Connection connection){
		
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");//コメントのID
			sql.append("comments.comment as comment, ");//コメントのコメント内容
			sql.append("comments.message_id as messageId, ");//コメントのメッセージID
			sql.append("comments.user_id as userId, ");//ユーザーID
			sql.append("users.name as name, ");//投稿者名
			sql.append("comments.created_date as created_date ");//コメントの登録日
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC");//得られたデータを投稿の順番に並べる
		
			ps = connection.prepareStatement(sql.toString());//SQL文を取得するためにStatementを取得
			
			ResultSet rs = ps.executeQuery();// SELECT文を実行するコード
		
			List<CommentList> ret = toUserCommentList(rs);
			return ret;
			}catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
	}

	private List<CommentList> toUserCommentList(ResultSet rs)
	throws SQLException {
		
		List<CommentList> ret = new ArrayList<CommentList>();//参照したものすべてをretというアレイリストにしまう
		try{
//			ResultSetを利用してSELECT結果を参照するコード
			while(rs.next()){
				int id = rs.getInt("id");
				String comment = rs.getString("comment");
				int messageId = rs.getInt("messageId");
				int userId =rs.getInt("userId");
				String name =rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
			
				CommentList comments = new CommentList();//
				
				comments.setId(id);
				comments.setComment(comment);
				comments.setMessageId(messageId);
				comments.setUserId(userId);
				comments.setName(name);
				comments.setCreated_date(createdDate);
				
				
				ret.add(comments);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	
}
