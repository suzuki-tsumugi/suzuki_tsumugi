package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserList;
import exception.SQLRuntimeException;

public class UserListDao { //支店名と支店番号の結合するためのdao
	public List<UserList> getUserList(Connection connection){
		PreparedStatement ps = null;
		try{

			String sql = "SELECT users.id as userId, users.loginId as loginId, users.name as name, "
					+ "users.password as password, users.confirmationPassword as confirmationPassword, "
					+ "users.permission as permission, branches.branch as branch, branches.branch_name as branchName, "
					+ "positions.position as position, positions.positionName as positionName "
					+ "FROM users "
					+ "INNER JOIN branches ON users.branch = branches.branch "
					+ "INNER JOIN positions ON users.position = positions.position";


			ps = connection.prepareStatement(sql.toString());//SQL文を取得するためにStatementを取得

			ResultSet rs = ps.executeQuery();// SELECT文を実行するコード

			List<UserList> ret = toUserList(rs);
			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserList> toUserList(ResultSet rs)
			throws SQLException{
		List<UserList> ret = new ArrayList<UserList>();
		try{
			while(rs.next()){
				int branch = rs.getInt("branch");
				String branchName = rs.getString("branchName");
				int position = rs.getInt("position");
				String positionName = rs.getString("positionName");
				int userId = rs.getInt("userId");
				String name = rs.getString("name");
				String loginId = rs.getString("loginId");
				String password = rs.getString("password");
				String confirmationPassword = rs.getString("confirmationPassword");
				String permission = rs.getString("permission");


				UserList userList = new UserList();

				userList.setBranch(branch);
				userList.setBranchName(branchName);
				userList.setPosition(position);
				userList.setPositionName(positionName);
				userList.setUserId(userId);
				userList.setName(name);
				userList.setLoginId(loginId);
				userList.setPassword(password);
				userList.setConfirmationPassword(confirmationPassword);
				userList.setPermission(permission);

				ret.add(userList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}