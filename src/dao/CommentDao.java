package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {//コメント内容を登録するためのDao
	public void insert(Connection connection,Comment comment){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments(");
			sql.append("user_id");
			sql.append(",message_id");
			sql.append(",comment");
			sql.append(",created_date");
			sql.append(",updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");//user_id
			sql.append(", ?");//message_id
			sql.append(", ?");//comment
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getMessageId());
			ps.setString(3, comment.getComment());//コメントに入力した値が入る
			
			ps.executeUpdate();//SQL文を実行
		} catch(SQLException e){
			throw new SQLRuntimeException (e);
					} finally {
						close(ps);
					}
	}

	public void delete(Connection connection, Comment commentDelete) {
		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM comments WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentDelete.getId());
			ps.executeUpdate();
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		
	}

}
