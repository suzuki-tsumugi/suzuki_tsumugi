package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
	//userテーブルに書き込み
	public void insert(Connection connection,User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("name");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", loginId");
			sql.append(", password");
			sql.append(", confirmationPassword");
			sql.append(", permission");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // 個人名
			sql.append(", ?");//支店
			sql.append(", ?"); // 部署.役職
			sql.append(", ?"); // ログインＩＤ
			sql.append(", ?"); // password
			sql.append(", ?"); // 確認用ＩＤ
			sql.append(", ?"); //ユーザー書き込み権限
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());//数値を文字列に変換

			ps.setString(1, user.getName()); //指定されたパラメータ(1)をuser.getName()の戻り値に設定
			ps.setString(2, Integer.toString(user.getBranch()));
			ps.setString(3, Integer.toString(user.getPosition()));
			ps.setString(4, user.getLoginId());
			ps.setString(5, user.getPassword());
			ps.setString(6, user.getConfirmationPassword());
			ps.setString(7, user.getPermission());

			ps.executeUpdate();//PreparedStatement オブジェクトの SQL 文を実行

		} catch (SQLException e) {//データベースアクセスエラーまたはその他のエラーに関する情報を提供する例外です
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginId, String password) {
		PreparedStatement ps = null;
		//ログインＩＤとパスワードのデータを元にＤＢから該当するユーザーの確認
		try{
			String sql = "SELECT * FROM users WHERE loginId = ? AND password = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();//上記のデータベースの結果セットを表すデータのテーブルrs

			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true){
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");//不正または不適切なときにメソッドが呼び出されたことを示します。
			}else{
				return userList.get(0);//リスト内の指定された位置（0）にある要素を返す
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}
	public  User getUserList(Connection connection, String userId) {
		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, userId);


			ResultSet rs = ps.executeQuery();//上記のデータベースの結果セットを表すデータのテーブルrs

			List<User> userList = toUserList(rs);

			return userList.get(0);
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException{

		List<User> ret = new ArrayList<User>();//Userクラスのアレイリストretを作成

		try{
			while(rs.next()){//カーソルを現在の位置から 1 行順方向に移動
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int position = rs.getInt("position");
				String loginId = rs.getString("loginId");
				String password = rs.getString("password");
				String confirmationPassword = rs.getString("confirmationPassword");
				String permission = rs.getString("permission");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setBranch(branch);
				user.setPosition(position);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setConfirmationPassword(confirmationPassword);
				user.setPermission(permission);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user); //アレイリストretに加える
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void update1(Connection connection,User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", loginId = ?");
			sql.append(", password = ?");
			sql.append(", confirmationPassword = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE id = ?"); // ユーザーIDで更新するレコードを特定。

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			ps.setInt(2,user.getBranch());
			ps.setInt(3, user.getPosition());
			ps.setString(4, user.getLoginId());
			ps.setString(5,user.getPassword());
			ps.setString(6, user.getConfirmationPassword());
			ps.setInt(7, user.getId());

			int count = ps.executeUpdate();

			if(count == 0){ //更新行数が0のときに例外を投げる。

				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {//データベースアクセスエラーまたはその他のエラーに関する情報を提供する例外です
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection,User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", loginId = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE id = ?"); // ユーザーIDで更新するレコードを特定。

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			ps.setInt(2,user.getBranch());
			ps.setInt(3, user.getPosition());
			ps.setString(4, user.getLoginId());
			ps.setInt(5, user.getId());

			int count = ps.executeUpdate();

			if(count == 0){ //更新行数が0のときに例外を投げる。

				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {//データベースアクセスエラーまたはその他のエラーに関する情報を提供する例外です
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



}



