package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.PostList;
import exception.SQLRuntimeException;

public class GetMessageDao {//投稿一覧をＤＢから取得するためのdao

	public List<PostList> getPostList(Connection connection,int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");//messagesのid
			sql.append("messages.subject as subject, ");//subject
			sql.append("messages.text as text, ");//text
			sql.append("messages.user_id as user_id, ");//user_id
			sql.append("messages.category as category, ");//category
			sql.append("users.name as name, ");//投稿者名
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");//ユーザーのidとメッセージのユーザー
			sql.append("ORDER BY created_date DESC limit " + num);//得られるデータの順序を投稿の降順にする

			ps = connection.prepareStatement(sql.toString());//SQL文を取得するためにStatementを取得

			ResultSet rs = ps.executeQuery();// SELECT文を実行するコード

			List<PostList> ret = toUserMessageList(rs);
			return ret;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<PostList> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<PostList> ret = new ArrayList<PostList>(); //参照したものすべてをretというアレイリストにしまう
		try{
			//			ResultSetを利用してSELECT結果を参照するコード
			while(rs.next()){
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				PostList message = new PostList();//
				message.setSubject(subject);
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<PostList> searchCategory(Connection connection,String searchWord){ //カテゴリ部分一致検索
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");//messagesのid
			sql.append("messages.subject as subject, ");//subject
			sql.append("messages.text as text, ");//text
			sql.append("messages.user_id as user_id, ");//user_id
			sql.append("messages.category as category, ");//category
			sql.append("users.name as name, ");//投稿者名
			sql.append("messages.created_date as created_date ");//投稿日
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");//ユーザーのidとメッセージのユーザー
			sql.append("WHERE category like ? ");//カテゴリ部分一致検索
			sql.append("ORDER BY created_date DESC ");//得られるデータの順序を投稿の降順にする

			ps = connection.prepareStatement(sql.toString());//SQL文を取得するためにStatementを取得

			ps.setString(1, "%"+searchWord+"%");

			ResultSet rs = ps.executeQuery();// SELECT文を実行するコード

			List<PostList> ret = toUserMessageList(rs);
			return ret;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<PostList> searchPosted(Connection connection, String searchPosted){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");//messagesのid
			sql.append("messages.subject as subject, ");//subject
			sql.append("messages.text as text, ");//text
			sql.append("messages.user_id as user_id, ");//user_id
			sql.append("messages.category as category, ");//category
			sql.append("users.name as name, ");//投稿者名
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");//ユーザーのidとメッセージのユーザー
			sql.append("WHERE messages.created_date like ? ");
			sql.append("ORDER BY messages.created_date DESC " );//得られるデータの順序を投稿の降順にする

			ps = connection.prepareStatement(sql.toString());//SQL文を取得するためにStatementを取得

			ps.setString(1, searchPosted +"%" );
			ResultSet rs = ps.executeQuery();// SELECT文を実行するコード

			List<PostList> ret = toUserMessageList(rs);
			return ret;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}


	}

}