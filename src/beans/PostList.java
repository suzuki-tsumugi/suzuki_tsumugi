package beans;

import java.io.Serializable;
import java.util.Date;

public class PostList implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
    private String subject;//件名
    private String text;//本文
    private String category;//カテゴリー
    private String name;//投稿者(名前)
    private int userId;//投稿
    private Date created_date;//投稿日時

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
    
}
