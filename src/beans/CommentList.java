package beans;

import java.io.Serializable;
import java.util.Date;

public class CommentList implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String comment;//コメント
	private String name;//投稿者(名前)
	private int messageId;//投稿とコメントを紐付けるためのID
	private int userId;//人と連携するためのid
	private Date created_date;//投稿日時
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
}
