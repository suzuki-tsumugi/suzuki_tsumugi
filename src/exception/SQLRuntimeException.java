package exception;

import java.sql.SQLException;

//データベースアクセスエラーまたはその他のエラーに関する情報を提供する例外


public class SQLRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SQLRuntimeException(SQLException cause) {
		super(cause);
	}

}
