package exception;

import java.io.IOException;//入出力処理の失敗、または割り込みの発生によって生成される例外の汎用クラス

public class IORuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public IORuntimeException(IOException cause) {
		super(cause);
	}

}
